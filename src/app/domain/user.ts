export class User {
  id: any
  name: string
  email: string
  password: string
  birthday: Date
  address: string
  postalCode: string
  orders: []
}
