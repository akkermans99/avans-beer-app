export class Packet {
  id: any
  name: string
  description: string
  beers: []
  numberOfBeers: number
}
