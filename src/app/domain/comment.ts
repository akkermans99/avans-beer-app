import { User } from './user'

export class Comment {
  content: string
  user: User
  rating: number
  id: any
}
