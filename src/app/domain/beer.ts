export class Beer {
  name: string
  description: string
  price: number
  alcoholPerc: number
  content: number
  brand: string
  imageUrl: string
  comments: []
  specie: string
}
