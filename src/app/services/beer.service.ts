import { Injectable } from '@angular/core'
import { Beer } from '../domain/beer'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { catchError, map, tap } from 'rxjs/operators'
import { Observable, of } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class BeerService {
  private beersUrl = 'https://avans-beer-api.herokuapp.com/api/beers'
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  constructor(private http: HttpClient) {}

  getBeers(): Observable<any> {
    console.log(this.beersUrl)
    return this.http.get<any>(this.beersUrl).pipe(
      tap(_ => console.log('fetched beers')),
      catchError(this.handleError<any>('getBeers', []))
    )
  }

  getBeerById(id: string): Observable<any> {
    const url = `${this.beersUrl}/${id}`
    console.log(url)
    return this.http.get<any>(url).pipe(
      tap(_ => console.log('fetched beer with id ' + id)),
      catchError(this.handleError<any>('getBeer'))
    )
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error) // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`)

      // Let the app keep running by returning an empty result.
      return of(result as T)
    }
  }
}
