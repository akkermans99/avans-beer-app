import { Component, OnInit } from '@angular/core'
import { Beer } from '../domain/beer'
import { Packet } from '../domain/packet'
import { BeerService } from '../services/beer.service'

@Component({
  selector: 'app-beer-list',
  templateUrl: './beer-list.component.html',
  styleUrls: ['./beer-list.component.scss']
})
export class BeerListComponent implements OnInit {
  beers: Beer[]

  constructor(private beerService: BeerService) {}

  ngOnInit() {
    this.getBeers()
  }

  getBeers(): void {
    this.beerService.getBeers().subscribe(beers => {
      console.log(beers)
      this.beers = beers.result
    })
  }
}
