import { Component, OnInit } from '@angular/core'
import { Beer } from '../domain/beer'
import { BeerService } from '../services/beer.service'
import { Location } from '@angular/common'
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-beer-details',
  templateUrl: './beer-details.component.html',
  styleUrls: ['./beer-details.component.scss']
})
export class BeerDetailsComponent implements OnInit {
  beer: Beer
  constructor(private beerService: BeerService, private route: ActivatedRoute, private location: Location) {}

  ngOnInit() {
    this.getBeer()
  }

  getBeer(): void {
    const id = this.route.snapshot.paramMap.get('id')
    this.beerService.getBeerById(id).subscribe(b => {
      console.log(b)
      this.beer = b.result[0]
    })
  }

  goBack(): void {
    this.location.back()
  }
}
